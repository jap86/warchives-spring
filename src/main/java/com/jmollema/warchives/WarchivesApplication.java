package com.jmollema.warchives;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarchivesApplication {

    public static void main(String[] args) {
        SpringApplication.run(WarchivesApplication.class, args);
    }

}
