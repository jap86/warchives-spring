package com.jmollema.warchives.controllers;

import com.jmollema.warchives.models.WarGame;
import com.jmollema.warchives.repositories.WarGameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
public class WarGameController extends Controller {
    private final String WAR_GAME_URL = URL + "/wargames";
    @Autowired
    private WarGameRepository warGameRepository;

    @GetMapping(value = WAR_GAME_URL)
    public List<WarGame> get() {
        return warGameRepository.findAll();
    }

    @GetMapping(WAR_GAME_URL + "/{name}")
    public WarGame get(@PathVariable("name") String name) {
        List<WarGame> warGames = get();
        return warGames.stream()
                .filter(warGame -> warGame.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @PostMapping(value = WAR_GAME_URL)
    @ResponseStatus(HttpStatus.OK)
    public void create(@RequestBody WarGame warGameRequest) {
        warGameRepository.save(warGameRequest);}
}
