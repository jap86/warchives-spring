package com.jmollema.warchives.controllers;

import com.jmollema.warchives.models.WarGameGenre;
import com.jmollema.warchives.repositories.WarGameGenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
public class WarGameGenreController extends Controller {
    private final String WAR_GAME_GENRE_URL = URL + "/wargamegenre";
    @Autowired
    private WarGameGenreRepository warGameGenreRepository;

    @GetMapping(value = WAR_GAME_GENRE_URL)
    public List<WarGameGenre> get() {return warGameGenreRepository.findAll();}

    @PostMapping(value = WAR_GAME_GENRE_URL)
    @ResponseStatus(HttpStatus.OK)
    public void create(@RequestBody WarGameGenre warGameGenre) {warGameGenreRepository.save(warGameGenre);}
}
