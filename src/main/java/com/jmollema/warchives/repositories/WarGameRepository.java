package com.jmollema.warchives.repositories;

import com.jmollema.warchives.models.WarGame;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WarGameRepository extends JpaRepository<WarGame, Long> {
}
