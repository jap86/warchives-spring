package com.jmollema.warchives.repositories;

import com.jmollema.warchives.models.WarGameGenre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WarGameGenreRepository extends JpaRepository<WarGameGenre, Long> {
}
